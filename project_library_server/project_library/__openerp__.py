{
    'name': "Library",
    'version': '1.0',
    'depends': ['base'],
    'author': "Okan Risvan",
    'description': """
    Library API
    """,
    'data': [
        'views/library_book.xml',
        'views/library_author.xml'
    ]
}