# encoding=utf8
import sys
from openerp import api, fields, models

reload(sys)
sys.setdefaultencoding('utf8')

# -*- coding: utf-8 -*-


class Operations(models.TransientModel):
    _name = 'library.operations'

    @api.model
    def server_receive_file(self, values):
        books = self.env['library.book'].search([])
        books.unlink()
        authors = self.env['library.author'].search([])
        authors.unlink()
        data = str(values.get('binary_data'))
        lines = data.split("\n")
        for line in lines:
            words = line.split(",")

            author_data = {
                'name': words[2],
                'surname': words[3],
                'date_of_birth': words[4],
            }

            author = self.env['library.author'].search([
                ('name', '=', author_data['name']),
                ('surname', '=', author_data['surname'])])

            if author.exists and len(author) == 1:
                author.write(author_data)
            else:
                author = self.env['library.author'].create(author_data)
            book = self.env['library.book'].create({
                'title': words[0],
                'classification': words[1],
                'author_ids': [(6, 0, [author.id])]
            })

            author.book_ids = [(4, book.id)]

        return True
