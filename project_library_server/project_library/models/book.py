# -*- coding: utf-8 -*-

from openerp import api, fields, models


class Book(models.Model):
    _name = 'library.book'
    _rec_name = 'title'

    title = fields.Char('Title', required=True)
    classification = fields.Selection([
        ('horror', 'Horror'),
        ('sci-fi', 'Sci-Fi'),
        ('bio', 'Biography')
    ])
    author_ids = fields.Many2many('library.author')
