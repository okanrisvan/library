# -*- coding: utf-8 -*-

from openerp import api, fields, models


class Author(models.Model):
    _name = 'library.author'

    name = fields.Char('Name', required=True)
    surname = fields.Char('Surname', required=True)
    date_of_birth = fields.Date('Date of birth', required=True)
    book_ids = fields.Many2many('library.book')
