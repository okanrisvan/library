import argparse
import json
import logging
import xmlrpclib

_logger = logging.getLogger(__name__)

url = 'http://localhost:8069'
db = 'db11'
username = 'admin'
password = '1'

common = xmlrpclib.ServerProxy('{}/xmlrpc/2/common'.format(url))
uid = common.authenticate(db, username, password, {})
models = xmlrpclib.ServerProxy('{}/xmlrpc/2/object'.format(url))


def create(args):
    if args.book == 'y' and args.title and args.classification:
        id = models.execute_kw(db, uid, password, 'library.book', 'create', [{
            'title': args.title,
            'classification': args.classification
        }])

    if args.name and args.surname and args.author == 'y':
        id = models.execute_kw(db, uid, password, 'library.author', 'create', [{
            'name': args.name,
            'surname': args.surname,
            'date_of_birth': args.date_of_birth
        }])


def change(args):
    if args.book == 'y' and args.title and args.classification:
        models.execute_kw(db, uid, password, 'library.book', 'write',
                          [[int(args.id)], {
                              'title': args.title,
                              'classification': args.classification,
                          }])

    if args.name and args.surname and args.author == 'y':
        models.execute_kw(db, uid, password, 'library.author', 'write',
                          [[int(args.id)], {
                              'name': args.name,
                              'surname': args.surname,
                              'date_of_birth': args.date_of_birth,
                          }])


def show(args):
    if args.book == "y":
        book = models.execute_kw(db, uid, password, 'library.book', 'read',
                                 [int(args.id)])

        print book.get('title')
        print book.get('classification')

    if args.author == "y":
        author = models.execute_kw(db, uid, password, 'library.author', 'read',
                                   [int(args.id)])

        print author.get('name')
        print author.get('surname')
        print author.get('date_of_birth')


def listing(args):
    if args.book == "y":
        ids = models.execute_kw(db, uid, password,
                                'library.book', 'search', [[]])

        lists = models.execute_kw(db, uid, password, 'library.book', 'read',
                                  [ids])
        print lists

    if args.author == "y":
        ids = models.execute_kw(db, uid, password,
                                'library.author', 'search', [[]])
        lists = models.execute_kw(db, uid, password, 'library.author', 'read',
                                  [ids])
        print lists


def csv_upload(args):
    with open(args.file, "rb") as handle:
        binary_data = xmlrpclib.Binary(handle.read())
        models.execute_kw(
            db, uid, password, 'library.operations', 'server_receive_file', [{
                'binary_data': binary_data
            }])


def author_upload(args):
    with open(args.file) as data_file:
        data = json.load(data_file)
        author = data["author"]
        for i in author:
            id = models.execute_kw(
                db, uid, password, 'library.author', 'create', [{
                    'name': i["name"],
                    'surname': i["surname"],
                    'date_of_birth': i["date_of_birth"]
                }])


parser = argparse.ArgumentParser()
subparsers = parser.add_subparsers()

new = subparsers.add_parser('new')
new.add_argument('--author')
new.add_argument('--name', help='author\'s name')
new.add_argument('--surname', help='author\'s surname')
new.add_argument('--date_of_birth')
new.add_argument('--id')
new.add_argument('--book')
new.add_argument('--title')
new.add_argument('--classification')
new.set_defaults(func=create)

update = subparsers.add_parser('update')
update.add_argument('--author')
update.add_argument('--name', help='author\'s name')
update.add_argument('--surname', help='author\'s surname')
update.add_argument('--date_of_birth')
update.add_argument('--id')
update.add_argument('--book')
update.add_argument('--title')
update.add_argument('--classification')
update.set_defaults(func=change)

get = subparsers.add_parser('get')
get.add_argument('--id')
get.add_argument('--book')
get.add_argument('--author')
get.set_defaults(func=show)

lists = subparsers.add_parser('lists')
lists.add_argument('--book')
lists.add_argument('--author')
lists.set_defaults(func=listing)

csv_uploader = subparsers.add_parser('csv_uploader')
csv_uploader.add_argument('--file')
csv_uploader.set_defaults(func=csv_upload)

author_uploader = subparsers.add_parser('author_uploader')
author_uploader.add_argument('--file')
author_uploader.set_defaults(func=author_upload)

if __name__ == '__main__':
    args = parser.parse_args()
    args.func(args)
